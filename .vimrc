set nocompatible

filetype plugin indent on
syntax on
set number
set scrolloff=3

set tabstop=4
set softtabstop=0
set shiftwidth=4
set copyindent
set autoindent
set smarttab

set showmatch
set showmode
set showcmd

set ruler
set laststatus=2
set display=lastline
set report=0

set fileencoding=utf-8
set termencoding=utf-8
set termguicolors
set background=dark

" See sensible.vim
set backspace=indent,eol,start
set hidden
set complete-=i
set nrformats-=octal
set wildmenu
set incsearch
" turn off search highlights
nnoremap <leader><space> :nohlsearch<CR>

" switch buffers
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>
nnoremap <C-X> :bdelete<CR>

" fast save and close
nmap <leader>w :w<CR>
nmap <leader>x :x<CR>
nmap <leader>q :q<CR>

" cursorline
set cursorline
autocmd InsertLeave,WinEnter * set cursorline
autocmd InsertEnter,WinEnter * set nocursorline

" Spell check
set spell
map <F6> :setlocal spell! spelllang=en_us<CR>

" split windows controll
" set mouse=n
tnoremap <A-h> <C-\><C-N><C-w>h
tnoremap <A-j> <C-\><C-N><C-w>j
tnoremap <A-k> <C-\><C-N><C-w>k
tnoremap <A-l> <C-\><C-N><C-w>l
inoremap <A-h> <C-\><C-N><C-w>h
inoremap <A-j> <C-\><C-N><C-w>j
inoremap <A-k> <C-\><C-N><C-w>k
inoremap <A-l> <C-\><C-N><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l
" switch splits
map <leader>h <C-w>H
map <leader>j <C-w>J
map <leader>k <C-w>K
map <leader>l <C-w>L

" Auto load file change
set autoread
autocmd FocusGained * silent! checktime

"latex
let g:tex_flavor = "latex"

"tab, indent and space
let &colorcolumn="80"
autocmd BufRead,BufNewFile *.tex setlocal lbr cc=0
autocmd BufRead,BufNewFile *.bib setlocal lbr cc=0
autocmd BufRead,BufNewFile *.md setlocal lbr cc=0
autocmd BufRead,BufNewFile *.txt setlocal lbr cc=0
autocmd BufRead,BufNewFile *.jsonnet setlocal expandtab
set list lcs=tab:>·,trail:·


" vim input method controller
let g:input_toggle = 0
let g:input_default = "xkb:us::eng"
let g:input_last = ""

function! Ibus2default()
    if g:input_toggle == 1
        let l:status = system("ibus engine")
        if g:input_default != l:status
            let g:input_last = l:status
            let l:a = system("ibus engine " . g:input_default )
        endif
    endif
endfunction

function! Ibus2last()
    if g:input_toggle == 1 && g:input_last != ""
        let l:a = system("ibus engine " . g:input_last )
    endif
endfunction

autocmd InsertLeave * call Ibus2default()
autocmd InsertEnter * call Ibus2last()
