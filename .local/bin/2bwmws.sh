#!/usr/bin/env bash
# display workspace status

# get the current workspace
ws=$( xprop -root _NET_CURRENT_DESKTOP | sed -e 's/_NET_CURRENT_DESKTOP(CARDINAL) = //' )

# icons
icons=(TERM WEB MEDIA MISC)

#  print workspaces to stdout
draw() {
    for i in {0..3}; do
        if [[ $i -eq $ws ]]
        then
            # current workspace
            echo -ne "%{R} ${icons[$i]} %{R-}"
        else
            echo -ne " ${icons[$i]} "
        fi
    done
}

draw
