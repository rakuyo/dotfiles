set nocompatible

filetype plugin indent on
syntax on
set number
set scrolloff=3

set tabstop=4
set softtabstop=0
set shiftwidth=4
set copyindent
set autoindent
set smarttab

set showmatch
" set showmode
set noshowmode " because of lightline
set showcmd
set hidden

set ruler
set laststatus=2
set display=lastline
set report=0

set fileencoding=utf-8
set termencoding=utf-8
set termguicolors
set background=dark

" turn off search highlights
nnoremap <leader><space> :nohlsearch<CR>

" change history
set undodir=~/.local/share/nvim/undodir
set undofile
set undolevels=1000
set undoreload=10000

" switch buffers
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>
nnoremap <C-X> :bdelete<CR>

" fast save and close
nmap <leader>w :w<CR>
nmap <leader>x :x<CR>
nmap <leader>q :q<CR>

" cursorline
set cursorline
autocmd InsertLeave,WinEnter * set cursorline
autocmd InsertEnter,WinLeave * set nocursorline

" Spell check
set spell
map <F6> :setlocal spell! spelllang=en_us<CR>

" split windows controll
" set mouse=n
tnoremap <A-h> <C-\><C-N><C-w>h
tnoremap <A-j> <C-\><C-N><C-w>j
tnoremap <A-k> <C-\><C-N><C-w>k
tnoremap <A-l> <C-\><C-N><C-w>l
inoremap <A-h> <C-\><C-N><C-w>h
inoremap <A-j> <C-\><C-N><C-w>j
inoremap <A-k> <C-\><C-N><C-w>k
inoremap <A-l> <C-\><C-N><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l
" switch splits
map <leader>h <C-w>H
map <leader>j <C-w>J
map <leader>k <C-w>K
map <leader>l <C-w>L

" Auto load file change
set autoread
autocmd FocusGained * silent! checktime

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')

" Make sure you use single quotes
Plug 'morhetz/gruvbox' "color scheme
Plug 'itchyny/lightline.vim'
Plug 'ap/vim-buftabline' "buffers at top
Plug 'tpope/vim-commentary' "comment
Plug 'tpope/vim-fugitive' "git
Plug 'tpope/vim-surround' "surround
Plug 'tpope/vim-repeat' "repeat last cmd for tpope plugs
Plug 'bfredl/nvim-miniyank'
Plug 'google/vim-jsonnet' "jsonnet
Plug 'stephpy/vim-yaml' "yaml
Plug 'davidhalter/jedi-vim' "auto complete
Plug 'dense-analysis/ale' "syntax check
Plug 'junegunn/vim-easy-align'
Plug 'junegunn/fzf'
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
  Plug 'deoplete-plugins/deoplete-jedi'
endif
let g:deoplete#enable_at_startup = 1
" Initialize plugin system
call plug#end()

"Ale
let g:ale_sign_error = '!!'
let g:ale_sign_warning = '>>'
let g:ale_sign_info = '=='
let g:ale_sign_style = '--'
" let g:ale_sh_shellcheck_options = '-S warning'

" Use ALE and also some plugin 'foobar' as completion sources for all code.
call deoplete#custom#option('sources', {
\ '_': ['ale', 'buffer', 'file'],
\})

let g:buftabline_indicators = 1
let g:buftabline_show = 2
let g:buftabline_numbers = 2

" lightline
let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified', 'spell' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }

"fzf
" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1
" let g:fzf_history_dir = '~/.local/share/fzf-history'
" Open files in horizontal split
" This is the default extra key bindings
nnoremap <silent> - :FZF<CR>

" easy-align
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" miniyank
map p <Plug>(miniyank-startput)
map P <Plug>(miniyank-startPut)

"latex
let g:tex_flavor = "latex"

"tab, indent and space
let &colorcolumn="80"
autocmd BufRead,BufNewFile *.tex setlocal lbr cc=0 expandtab
autocmd BufRead,BufNewFile *.bib setlocal lbr cc=0
autocmd BufRead,BufNewFile *.md setlocal lbr cc=0
autocmd BufRead,BufNewFile *.txt setlocal lbr cc=0
autocmd BufRead,BufNewFile *.jsonnet setlocal expandtab
autocmd BufRead,BufNewFile *.sh setlocal expandtab
set list lcs=tab:>·,trail:·

"colo confs
colorscheme gruvbox
" let g:gruvbox_guisp_fallback = "bg"
" hi Normal guibg=NONE ctermbg=NONE

nmap <leader>1 <Plug>BufTabLine.Go(1)
nmap <leader>2 <Plug>BufTabLine.Go(2)
nmap <leader>3 <Plug>BufTabLine.Go(3)
nmap <leader>4 <Plug>BufTabLine.Go(4)
nmap <leader>5 <Plug>BufTabLine.Go(5)
nmap <leader>6 <Plug>BufTabLine.Go(6)
nmap <leader>7 <Plug>BufTabLine.Go(7)
nmap <leader>8 <Plug>BufTabLine.Go(8)
nmap <leader>9 <Plug>BufTabLine.Go(9)
nmap <leader>0 <Plug>BufTabLine.Go(-1)
